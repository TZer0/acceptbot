from pony.orm import *

db = Database()


def setupDb(name):
	set_sql_debug(False)
	db.bind(provider='sqlite', filename=name, create_db=True)
	db.generate_mapping(create_tables=True)
	commit()


class AcceptRole(db.Entity):
	guild_id = PrimaryKey(int, size=64)
	id = Required(int, size=64)  # role


class WelcomeChannel(db.Entity):
	guild_id = PrimaryKey(int, size=64)
	id = Required(int, size=64)  # channel


class WelcomeMessage(db.Entity):
	guild_id = PrimaryKey(int, size=64)
	message = Required(str)
