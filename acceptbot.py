from datetime import datetime
from disco.bot import Plugin
from disco.bot import parser
from disco.types.message import MessageEmbed
from disco.types.channel import MessageIterator
from db import *
from disco.types.permissions import Permissible, PermissionValue, Permissions
from disco.types.base import Unset
import disco.util.snowflake as snowflake

from datetime import timedelta, datetime



class AcceptBot(Plugin):
	acceptTerms = ["accept", "rules", "dang", "agree"]
	rejectTerms = ["welcome", "acceptall", "No users accepted rules."]

	def __init__(self, bot, config):
		Plugin.__init__(self, bot=bot, config=config)
		if not self.client.config.configured_db:
			setupDb(self.client.config.db)

	def error(self, message):
		print(message)

	@db_session
	@Plugin.command('confrole', '[id:str...]')
	def confRole(self, event, id):
		if id == None:
			return
		if not self.isMod(event.author, event.guild):
			return

		delete(acceptRole for acceptRole in AcceptRole if acceptRole.guild_id == event.guild.id)
		commit()
		self.addToCollection(AcceptRole, int(id), event)
		event.msg.reply("Set")

	@db_session
	@Plugin.command('setwelcomemessage', '[msg:str...]')
	def setWelcomeMessage(self, event, msg=None):
		if msg == None:
			return
		if not self.isMod(event.author, event.guild):
			return

		delete(welcomeMessage for welcomeMessage in WelcomeMessage if welcomeMessage.guild_id == event.guild.id)
		commit()
		welcomeMessage = WelcomeMessage(guild_id=event.guild.id, message=msg)
		commit()
		event.msg.reply("Set")

	@db_session
	@Plugin.command('setwelcomechannel')
	def setWelcomeChannel(self, event):
		if not self.isMod(event.author, event.guild):
			return
		delete(welcomeChannel for welcomeChannel in WelcomeChannel if welcomeChannel.guild_id == event.guild.id)
		commit()
		self.addToCollection(WelcomeChannel, event.channel.id, event)
		event.msg.reply("Set")

	@db_session
	def getPreconditions(self, event):
		acceptRole = self.getSetting(AcceptRole, event.guild.id)
		if acceptRole == None:
			event.msg.reply("Acceptrole not set")
			return None, None, None
		welcomeChannel = self.getSetting(WelcomeChannel, event.guild.id)
		if welcomeChannel == None:
			event.msg.reply("Welcome channel not set!")
			return None, None, None
		welcomeMessage = self.getSetting(WelcomeMessage, event.guild.id)
		return acceptRole, welcomeChannel, welcomeMessage

	@db_session
	@Plugin.command('acceptAll', '[id:str...]')
	@Plugin.command('aa', '[id:str...]')
	def acceptAll(self, event, id=None):
		if id == None:
			return
		if not self.isMod(event.author, event.guild):
			return

		acceptRole, welcomeChannel, welcomeMessage = self.getPreconditions(event)
		if acceptRole is None or welcomeChannel is None:
			return

		id = id.split("-")[-1]
		selectedMsg = snowflake.to_datetime(snowflake.to_snowflake(int(id)))
		weekAgo = datetime.now() - timedelta(days=7)
		if not weekAgo < selectedMsg:
			event.msg.reply("Can't acceptall messages older than 1 week.")
			return

		users = []
		acceptedMessages = []

		for msgs in event.channel.messages_iter(after=int(id) - 1,
		                                        before=event.msg.id - 1,
		                                        bulk=True, direction=MessageIterator.Direction.DOWN):
			for msg in msgs:
				if msg.author.id == event.author.id:
					continue
				if msg.author.id not in event.guild.members:
					continue
				if msg.author in users or any(
						role == acceptRole.id for role in event.guild.members[msg.author.id].roles):
					continue
				if not any(reject in msg.content.lower() for reject in self.rejectTerms) and any(
						accept in msg.content.lower() for accept in self.acceptTerms):
					users.append(msg.author)
					acceptedMessages.append(msg)

		for acceptedMessage in acceptedMessages:
			acceptedMessage.add_reaction('✅')

		if len(users) == 0:
			event.msg.reply("No users accepted rules.")
			return

		self.acceptMembers(event, users, acceptRole, welcomeChannel, welcomeMessage)
		event.msg.delete()

	def acceptMembers(self, event, users, acceptRole, welcomeChannel, welcomeMessage):
		msg = ""

		for user in users:
			if len(msg) > 0:
				msg += ", "
			event.guild.members[user.id].add_role(acceptRole.id)
			msg += "<@" + str(user.id) + ">"

		if welcomeMessage is not None:
			msg = welcomeMessage.message + "\n" + msg
		embed = MessageEmbed()
		embed.title = "You're in!"
		embed.add_field(name="Information", value="The following mod let you in: <@" + str(event.author.id) + ">")
		event.guild.channels[welcomeChannel.id].send_message(msg, embed=embed)

	@db_session
	@Plugin.command('acceptOne', '[id:str...]')
	@Plugin.command('a1', '[id:str...]')
	def acceptOne(self, event, id=None):
		if id == None:
			return
		if not self.isMod(event.author, event.guild):
			return
		acceptRole, welcomeChannel, welcomeMessage = self.getPreconditions(event)
		if acceptRole == None or welcomeChannel == None:
			return
		intId = int(id)
		if intId in event.guild.members:
			searchFrom = snowflake.from_datetime(datetime.now() - timedelta(days=1))
			found = False
			for msgs in event.channel.messages_iter(after=searchFrom, bulk=True,
			                                        direction=MessageIterator.Direction.UP):
				for msg in msgs:
					if msg.author.id == intId:
						msg.add_reaction('✅')
						found = True
						break
				if found:
					break
			self.acceptMembers(event, [event.guild.members[intId]], acceptRole, welcomeChannel, welcomeMessage)
		event.msg.delete()

	@db_session
	@Plugin.command('!help')
	def helpHandler(self, event):
		if not self.isMod(event.author, event.guild):
			return

		embed = MessageEmbed()
		embed.title = "Commands"

		embed.add_field(name="!help", value="Gives you this message")
		embed.add_field(name="acceptall [channelId-]messageId", value="Scans messages from and including the given "
		                                                              "message ID and lets all users into the server "
		                                                              "that have the following words in their messages "
		                                                              "since that message: " + str(self.acceptTerms) +
		                                                              " and none of these terms: " + str(
			self.rejectTerms) + "\nIf you get an error, you've copied the user ID.\nchannelId is ignored.")
		embed.add_field(name="aa [channelId-]messageId", value="Alias for acceptall")
		embed.add_field(name="acceptone userid", value="Lets a user into the server regardless of what they've written "
		                                               "- use this in case someone doesn't get matched by the other "
		                                               "command. Doesn't leave any reacts.")
		embed.add_field(name="a1 [channelId-]messageId", value="Alias for acceptone")
		embed.add_field(name="setwelcomechannel", value="Sets the welcome channel to the channel this was sent in")
		embed.add_field(name="confrole ROLEID", value="Sets the memberrole to the ROLEID")
		embed.add_field(name="setwelcomemessage MESSAGE", value="Sets the welcome message to MESSAGE")
		embed.add_field(name="How to get ids", value="User Settings -> Appearance -> enable Developer Mode\nRight "
		                                             "click on message -> Copy ID. You can also hover a message, "
		                                             "hold shift and click the ID-button. No need to edit it as "
		                                             "acceptAll ignores everything before the dash.")
		embed.add_field(name="Quick video guide - setting up + letting people in", value="https://www.youtube.com"
		                                                                                 "/watch?v=S6FX56gEu8k")
		embed.add_field(name="You can also check my Discord-notes here", value="https://tzer0.github.io/discord.html")
		embed.add_field(name="Support me on Patreon", value="https://www.patreon.com/TZer0")

		event.msg.reply(embed=embed)

	# util methods

	def isMod(self, member, guild):
		return guild.get_permissions(member).can(Permissions.MANAGE_ROLES.value)

	def getExistingOrEmpty(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return collection(id=id, guild_id=guild_id)

	def getObjectOrNone(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return None

	def getSetting(self, collection, guild_id):
		if collection.exists(guild_id=guild_id):
			return collection.get(guild_id=guild_id)
		else:
			return None

	def addToCollection(self, collection, id, event):
		if not self.isMod(event.author, event.guild):
			return
		self.getExistingOrEmpty(collection, id, event.guild.id)
		commit()

	def removeFromCollection(self, collection, id, event):
		if not self.isMod(event.author, event.guild):
			return

		removeRow = self.getObjectOrNone(collection, id, event.guild.id)
		if (removeRow != None):
			removeRow.delete()
			commit()
			return True
		return False
