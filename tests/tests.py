import unittest
import sys
from datetime import datetime

from disco.client import ClientConfig, Client
from disco.bot.command import Command
from disco.bot.bot import Bot
from disco.types.permissions import Permissions, PermissionValue
from disco.types.guild import AuditLogActionTypes

import disco.util.snowflake as snowflake
from datetime import timedelta, datetime

sys.path.append('..')

from pony.orm import *

import acceptbot
import db

class MockBot(Bot):
    @property
    def commands(self):
        return getattr(self, '_commands', [])

class Object(object):
	pass

class Guild:
	def __init__(self, id, name, members, roles, channels):
		self.id = id
		self.name = name
		self.members = members
		self.roles = roles
		self.channels = channels
		self.audit = []
	def get_audit_log_entries(self):
		return self.audit
	def setAudit(self, audit):
		self.audit = audit
	def get_permissions(self, user):
		if 1 in  user.roles:
			return PermissionValue(Permissions.MANAGE_ROLES.value)
		else:
			return PermissionValue(Permissions.SPEAK.value)
	def can(self, user, *args):
		return 1 in user.roles

class Channel:
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.msg = None
		self.embed = None
		self.messages = []
	def setMessages(self, messages):
		self.messages = messages
	def send_message(self, msg=None, embed=None):
		self.msg = msg
		self.embed = embed
	def messages_iter(self, **kwargs):
		return [self.messages]

class Member:
	def __init__(self, id, name, roles = []):
		self.id = id
		self.name = name
		self.roles = roles
		self.removed_roles = []
		self.added_roles = []
	def __str__(self):
		return self.name
	def get_scan_variants(self):
		return [str(self.id), self.name]
	def setGuild(self, guild):
		self.guild = guild
		self.guild_id = guild.id
	def remove_role(self, id):
		self.removed_roles.append(id)
	def add_role(self, id):
		self.added_roles.append(id)

class Event(object):
	def __init__(self, author, channel, guild):
		self.channel = channel
		self.rep = None
		self.embed = None
		self.guild = guild
		self.msg = Object
		self.msg.reply = self.reply
		self.msg.delete = self.deleteMeta
		self.msg.id = 10
		self.member = self.author = author
		self.id = 10
		self.deleteCalled = False
	
	def deleteMeta(self):
		self.deleteCalled = True
		
	def reply(self, rep=None, embed=None):
		self.rep = rep
		self.embed = embed

class MessageEvent(Event):
	def __init__(self, author, channel, guild, content):
		super().__init__(author, channel, guild)
		self.content = content
		self.delete = self.deleteMeta
		self.emojis = []
	
	def add_reaction(self, emoji):
		self.emojis.append(emoji)


class GuildEvent(object):
	def __init__(self, member, guild):
		self.member = member
		self.guild = guild
        
class Role:
	def __init__(self, id, name):
		self.id = id
		self.name = name

def getIdDict(l):
	ret = {}
	for el in l:
		ret[el.id] = el
	return ret

class AcceptBotTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		db.setupDb("test.sqlite")
	def setUp(self):
		self.config = ClientConfig(
			{'config': 'TEST_TOKEN', 'configured_db': True}
		)
		self.client = Client(self.config)
		self.bot = MockBot(self.client)
		self.plugin = acceptbot.AcceptBot(self.bot, self.config)
		self.setupStructure()
	
	def tearDown(self):
		db.db.drop_all_tables(with_all_data=True)
		db.db.create_tables()
		
	@db_session	
	def setupStructure(self):
		self.testRole = Role(4, "testrole")
		self.member = Role(2, "member test2")
		self.admin = Role(1, "admin test1")
		role_dict = getIdDict([self.admin, self.member, self.testRole])
		self.member1 = Member(600, "m1", roles=[self.admin.id])
		self.member2 = Member(700, "m2", roles=[self.member.id])

		self.good_channel = Channel(13, "test")
		self.another_channel = Channel(14, "test2")
		
		self.good_guild = Guild(6, "g6", getIdDict([self.member1, self.member2]), role_dict, getIdDict([self.another_channel, self.good_channel]))
		self.good_channel.setMessages([MessageEvent(self.member2, self.good_channel, self.good_guild, "I Accept")])
		self.another_channel.setMessages([MessageEvent(self.member2, self.good_channel, self.good_guild, "blugh")])
		
		self.member1.setGuild(self.good_guild)
		self.member2.setGuild(self.good_guild)
		
		self.plugin.state.guilds = {self.good_guild.id : self.good_guild}
	
	def getOkayMessage(self):
		return MessageEvent(self.member2, self.good_channel, self.good_guild, "ping")
	
	@db_session
	def test_set_conf_role_bad(self):
		msg = MessageEvent(self.member2, self.good_channel, self.good_guild, '123')
		self.plugin.confRole(msg, '123')
		self.assertTrue(self.plugin.getObjectOrNone(db.AcceptRole, 123, 6) == None)
		
	
	@db_session
	def test_set_conf_role_good(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '124')
		self.plugin.confRole(msg, '124')
		self.assertTrue(self.plugin.getObjectOrNone(db.AcceptRole, 124, 6) != None)
		self.assertEqual("Set", msg.rep)
		
	@db_session
	def test_set_welcome_bad(self):
		msg = MessageEvent(self.member2, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.assertTrue(self.plugin.getObjectOrNone(db.WelcomeChannel, self.good_channel.id, 6) == None)
		
	
	@db_session
	def test_set_welcome_good(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.assertTrue(self.plugin.getObjectOrNone(db.WelcomeChannel, self.good_channel.id, 6) != None)
		self.assertEqual("Set", msg.rep)

	@db_session
	def test_set_welcome_message_bad_user(self):
		msg1 = MessageEvent(self.member2, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeMessage(msg1, "test1")
		self.assertIsNone(self.plugin.getSetting(db.WelcomeMessage, self.good_guild.id))
		self.assertIsNone(msg1.rep)

	@db_session
	def test_set_welcome_message_good_change(self):
		msg1 = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeMessage(msg1, "test1")
		msg2 = MessageEvent(self.member1, self.another_channel, self.good_guild, '')
		self.plugin.setWelcomeMessage(msg2, "test2")
		self.assertTrue(self.plugin.getSetting(db.WelcomeMessage, self.good_guild.id).message == "test2")
		self.assertEqual("Set", msg1.rep)
		self.assertEqual("Set", msg2.rep)

	@db_session
	def test_set_welcome_good_change(self):
		msg1 = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg1)
		msg2 = MessageEvent(self.member1, self.another_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg2)
		self.assertTrue(self.plugin.getObjectOrNone(db.WelcomeChannel, self.another_channel.id, 6) != None)
		self.assertEqual("Set", msg1.rep)
		self.assertEqual("Set", msg2.rep)
	
	@db_session
	def test_acceptall_bad_user(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		msg = MessageEvent(self.member2, self.good_channel, self.good_guild, '')
		self.plugin.acceptAll(msg, "1000")
		
		self.assertEqual(self.good_channel.msg, None)
		self.assertEqual(msg.rep, None)

	@db_session
	def test_acceptall_missing_role(self):
		msg = MessageEvent(self.member2, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.acceptAll(msg, "1000")
		
		self.assertEqual(msg.rep, "Acceptrole not set")

	@db_session
	def test_acceptall_missing_welcome_channel(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.confRole(msg, '4')
		self.plugin.acceptAll(msg, "1000")
		
		self.assertEqual(msg.rep, "Welcome channel not set!")

	@db_session
	def test_acceptall_no_accept(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.confRole(msg, '4')
		msg = MessageEvent(self.member1, self.another_channel, self.good_guild, '')
		self.plugin.acceptAll(msg, str(snowflake.from_datetime(datetime.now())))
		
		self.assertEqual(msg.rep, "No users accepted rules.")
		
	@db_session
	def test_acceptall_accepted_too_old(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.confRole(msg, '4')
		self.plugin.acceptAll(msg, "1000")
		
		self.assertEqual(msg.rep, "Can't acceptall messages older than 1 week.")
		self.assertFalse(4 in self.member2.added_roles)
		self.assertEqual(self.good_channel.msg, None)
		self.assertFalse(msg.deleteCalled)

	@db_session
	def test_acceptall_accepted_with_welcome_message(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.setWelcomeMessage(msg, "A welcome message")
		self.plugin.confRole(msg, '4')
		msg = Event(self.member1, self.good_channel, self.good_guild)
		self.plugin.acceptAll(msg, "123-" + str(snowflake.from_datetime(datetime.now())))

		self.assertTrue(4 in self.member2.added_roles)
		self.assertTrue("<@700>" in self.good_channel.msg)
		self.assertTrue("A welcome message" in self.good_channel.msg)
		self.assertTrue(self.good_channel.embed != None)
		self.assertTrue(msg.deleteCalled)
		self.assertTrue(len(self.good_channel.messages[0].emojis) == 1)

	@db_session
	def test_acceptall_accepted(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.confRole(msg, '4')
		msg = Event(self.member1, self.good_channel, self.good_guild)
		self.plugin.acceptAll(msg, "123-"+str(snowflake.from_datetime(datetime.now())))
		
		self.assertTrue(4 in self.member2.added_roles)
		self.assertEqual(self.good_channel.msg, "<@700>")
		self.assertTrue(self.good_channel.embed != None)
		self.assertTrue(msg.deleteCalled)
		self.assertTrue(len(self.good_channel.messages[0].emojis) == 1)
		
	@db_session
	def test_acceptall_accept_fail(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.confRole(msg, '4')
		self.good_channel.setMessages([MessageEvent(self.member2, self.good_channel, self.good_guild, "I Accept welcome")])
		msg = Event(self.member1, self.good_channel, self.good_guild)
		self.plugin.acceptAll(msg, str(snowflake.from_datetime(datetime.now())))

		self.assertEqual(self.good_channel.msg, None)
		self.assertFalse(4 in self.member2.added_roles)
		self.assertEqual(msg.rep, "No users accepted rules.")
		self.assertFalse(msg.deleteCalled)

	@db_session
	def test_acceptall_accepted_has_role_fail(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.confRole(msg, '4')
		self.member2.roles.append(self.testRole.id)
		self.plugin.acceptAll(msg, str(snowflake.from_datetime(datetime.now())))

		self.assertEqual(self.good_channel.msg, None)
		self.assertFalse(4 in self.member2.added_roles)
		self.assertEqual(msg.rep, "No users accepted rules.")
		self.assertFalse(msg.deleteCalled)
		
	@db_session
	def test_acceptone(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		self.plugin.confRole(msg, '4')
		self.plugin.acceptOne(msg, str(self.member2.id))
		
		self.assertTrue(4 in self.member2.added_roles)
		self.assertEqual(self.good_channel.msg, "<@700>")
		self.assertTrue(self.good_channel.embed != None)
		self.assertTrue(msg.deleteCalled)
		self.assertTrue(len(self.good_channel.messages[0].emojis) == 1)
		
			
	@db_session
	def test_acceptone_bad_mod(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		msg = MessageEvent(self.member2, self.good_channel, self.good_guild, '')
		self.plugin.acceptOne(msg, str(self.member2.id))
		
		self.assertEqual(self.good_channel.msg, None)
		self.assertEqual(msg.rep, None)
		
	@db_session
	def test_acceptone_bad_user(self):
		msg = MessageEvent(self.member1, self.good_channel, self.good_guild, '')
		self.plugin.setWelcomeChannel(msg)
		msg = MessageEvent(self.member2, self.good_channel, self.good_guild, '')
		self.plugin.acceptOne(msg, '999999')
		
		self.assertEqual(self.good_channel.msg, None)
		self.assertEqual(msg.rep, None)

		
		
		
		
		
if __name__ == '__main__':
	unittest.main()
